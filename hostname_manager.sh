#!/bin/sh
#
# Script to manage the hostname on a Slackware Linux host
#
# Copyright (c) 2019, Chris Abela, Malta
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

USAGE="Usage:\n
hostname_generator [options] [argument]\n
\n
-c            Create a backup of /etc/HOSTNAME and /etc/hosts in\n
              /var/lib/hostname_manager\n
              May only be used in conjunction with -v\n
-d <domain>   Set a fixed domain in the argument.\n
              May only be used in conjuction with -n and -v\n
-h            Display this information.\n
-n <hostname> Set a hostname in argument.\n
              May only be used in conjuction with -d and -v\n
-r            Set random hostname and domain. The values will be between 4 and\n
              20 characters. Pass values to MIN and MAX respectively to\n
              override.\n
              May only be used in conjuction with -v\n
-s            Set default hostname and domain. The default hostname is darkstar,\n
              pass a value to DEFAULT_HN to override. The default domain is\n
              example.net, pass a value to DEFAULT_DOMAIN to override\n
              May only be used in conjuction with -v\n
-v            verbose\n
-x            Restore a backup from of /etc/HOSTNAME and /etc/hosts from\n
              /var/lib/hostname_manager\n
              May only be used in conjuction with -v\n
\n
Pass a value to ETCHOSTNAME and ETCHOST to override the /etc/HOSTNAME and\n
/etc/hosts respectively.\n
Pass a value to ARCHIVE to override /var/lib/hostname_manager\n
\n
A dialog interactive menu will run if called without any arguments. Pass a value\n
for DIALOG for a different dialog application (e.g. Xdialog).\n
\n"

# Users can pass a diffent value for these variables:
ARCHIVE="${ARCHIVE:-/var/lib/hostname_manager}"
DEFAULT_HN="${DEFAULT_HN:-darkstar}"
DEFAULT_DOMAIN="${DEFAULT_DOMAIN:-example.net}"
ETCHOSTS="${ETCHOSTS:-/etc/hosts}"
ETCHOSTNAME="${ETCHOSTNAME:-/etc/HOSTNAME}"
MAX=${MAX:-20}
MIN=${MIN:-4}

# User should not touch these:
HN="$( hostname )"
DOMAIN="$( hostname -f | sed "s/$HN.//1" )" 
NEWHOSTNAME="$HN" # Initial value
NEWDOMAIN="$DOMAIN" # Initial value
MOD=$(( MAX - MIN ))
U=$( echo "$USAGE" | sed 's/\\n//g' )
backup=off
change=off # If user wants to change the hostname/domain; change=on
default=off
exit=off
fixed=of # If user wants a specific hostname or domain; fixed=on
r=off # If user wants a random hostname and domain; r=on
restore=off
verbose=off

check(){
  for FILE in "$ETCHOSTS" "$ETCHOSTNAME"; do
    if [ ! -r "$FILE" ]; then
      echo "$FILE was not found"
      exit 2
    fi
  done
}

error_msg() {
  echo "Illegal combination of switches."
  echo -e "$U"
  exit 2
}

set_hostname() {
  echo "${NEWHOSTNAME}.${NEWDOMAIN}" > $ETCHOSTNAME
  sed -i "s/ ${HN}.${DOMAIN}/ ${NEWHOSTNAME}.${NEWDOMAIN}/g
    s/	${HN}.${DOMAIN}/	${NEWHOSTNAME}.${NEWDOMAIN}/g
    s/${HN}$/$NEWHOSTNAME/g" \
    $ETCHOSTS
}

random() {
  LENGTH=$(( $RANDOM % $MOD + $MIN ))
  unset R
  RCOUNT=0
  while [ $RCOUNT -le "$LENGTH" ] ;do
    R=$R$( mkpasswd | sed 's/[^a-zA-Z]//g' )
    RCOUNT=$( echo "$R" | wc -c )
  done
  # R has more or equal characters than MIN and less or equal than MAX
  R=$( echo $R | cut -c 1-$LENGTH ) 
}

backup_dialog () {
  # Keep temporary storage
   ARCHIVETMP="$ARCHIVE"
   ETCHOSTNAMETMP="$ETCHOSTNAME"
   ETCHOSTSTMP="$ETCHOSTS"
   # Confirm the ETCHOSTNAME
   if ! ETCHOSTNAME="$( "$DIALOG" --title "Backup" --inputbox "Backup the HOSTNAME file" 6 40 "$ETCHOSTNAME" 2>&1 1>&3 )"; then 
     ETCHOSTNAME="$ETCHOSTNAMETMP"
     return 1
   fi
   while [ ! -r "$ETCHOSTNAME" ]; do
     "$DIALOG" --title "Backup" --msgbox "$ETCHOSTNAME was not found" 6 40
     # As ETCHOSTNAME was not found, we retrieve the original value
     ETCHOSTNAME="$ETCHOSTNAMETMP"
     ETCHOSTNAME="$( "$DIALOG" --title "Backup" --inputbox "Backup the HOSTNAME file" 6 40 "$ETCHOSTNAME" 2>&1 1>&3 )" || \
       return 1
   done  
   # Confirm the ETCHOSTS
   if ! ETCHOSTS="$( "$DIALOG" --title "Backup" --inputbox "Backup the hosts file" 6 40 "$ETCHOSTS" 2>&1 1>&3 )"; then
     ETCHOSTS="$ETCHOSTSTMP"
     return 1
   fi
   while [ ! -r "$ETCHOSTS" ]; do
     "$DIALOG" --title "Backup" --msgbox "$ETCHOSTS was not found" 6 40
     # As ETCHOSTS was not found, we retrieve the original value
     ETCHOSTS="$ETCHOSTSTMP"
     ETCHOSTS="$( "$DIALOG" --title "Backup" --inputbox "Backup the hosts file" 6 40 "$ETCHOSTS" 2>&1 1>&3 )" || \
       return 1
   done
   # Confirm the ARCHIVE
   if ! ARCHIVE="$( "$DIALOG" --title "Backup" --inputbox "Archive Directory" 6 40 "$ARCHIVE" 2>&1 1>&3 )"; then 
     ARCHIVE="$ARCHIVETMP"
     return 1
   fi
   # Final Global Confirmation
   "$DIALOG" --title "Backup" --yesno "Are you sure you want to Backup $ETCHOSTNAME and $ETCHOSTS in ${ARCHIVE}?" 8 40 || \
     return 1
}

restore_dialog () {
  # Keep temporary storage
   ARCHIVETMP="$ARCHIVE"
   ETCHOSTNAMETMP="$ETCHOSTNAME"
   ETCHOSTSTMP="$ETCHOSTS"
   # Confirm the ARCHIVE
   if ! ARCHIVE="$( "$DIALOG" --title "Restore" --inputbox "Archive Directory" 6 40 "$ARCHIVE" 2>&1 1>&3 )"; then
     ARCHIVE="$ARCHIVETMP"
     return 1 
   fi
   while [ ! -d "$ARCHIVE" ]; do
     "$DIALOG" --title "Restore" --msgbox "$ARCHIVE was not found" 6 40
     # As ARCHIVE was not found, we retrieve the original value
     ARCHIVE="$ARCHIVETMP"
     ARCHIVE="$( "$DIALOG" --title "Restore" --inputbox "ARCHIVE directory" 6 40 "$ARCHIVE" 2>&1 1>&3 )" || \
       return 1
   done
   # Confirm the ETCHOSTNAME
   if ! ETCHOSTNAME="$( "$DIALOG" --title "Restore" --inputbox "Restore the HOSTNAME file" 6 40 "$ETCHOSTNAME" 2>&1 1>&3 )"; then 
     ETCHOSTNAME="$ETCHOSTNAMETMP"
     return 1
   fi
   # Confirm the ETCHOSTS
   if ! ETCHOSTS="$( "$DIALOG" --title "Restore" --inputbox "Restore the hosts file" 6 40 "$ETCHOSTS" 2>&1 1>&3 )"; then
     ETCHOSTS="$ETCHOSTSTMP"
     return 1
   fi
   # Final Global Confirmation
   "$DIALOG" --title "Restore" --yesno "Are you sure you want to Restore $ETCHOSTNAME and $ETCHOSTS from ${ARCHIVE}?" 8 40 || \
     return 1
}

while getopts cd:n:hrsvx arguments; do
  case $arguments in
    c)
      check
      backup=on
      ;;
    d)
      check
      change=on
      fixed=on
      NEWDOMAIN="$OPTARG"
      ;;
    h)
      echo -e "$U"
      ;;
    n)
      check
      change=on
      fixed=on
      NEWHOSTNAME="$OPTARG"
      ;;
    s)
      check
      change=on
      default=on
      NEWHOSTNAME="$DEFAULT_HN"
      NEWDOMAIN="$DEFAULT_DOMAIN"
      ;;
    r)
      check
      change=on
      r=on
      random
      NEWHOSTNAME="$R"
      random
      NEWDOMAIN="$R"
      ;;
    v)
      verbose=on
      ;;
    x)
      restore=on
      ;;
    :)
      echo "You forgot an argument for the switch."
      echo -e "$U"
      exit
      ;;
    \?)
      echo "$OPTARG is not a valid switch"
      echo -e "$U"
      exit
      ;;
    esac
done
shift $(($OPTIND-1))

# Ensure user has chosen sane options
COUNT=0
for OPT in backup fixed default r restore; do
  [ $( eval "echo \$$OPT" ) == on  ] && let 'COUNT+=1'
done
[ $COUNT -gt 1 ] && error_msg

if [ "$backup" == on ]; then
  mkdir -p $ARCHIVE
  cp "$ETCHOSTNAME" "$ARCHIVE/ETCHOSTNAME"
  cp "$ETCHOSTS" "$ARCHIVE/ETCHOSTS"
  if [ "$verbose" == on ]; then
    echo "  cp $ETCHOSTNAME $ARCHIVE/ETCHOSTNAME"
    echo "  cp $ETCHOSTS $ARCHIVE/ETCHOSTS"
  fi
  exit
fi 

if [ "$restore" == on ]; then
  for X in "$ARCHIVE/ETCHOSTNAME" "$ARCHIVE/ETCHOSTS"; do
    if  [ ! -r "$X" ]; then
      echo "$X was not found"
      exit 2
    fi
   done
  if [ "$verbose" == on ]; then
    echo "  cp $ARCHIVE/ETCHOSTNAME $ETCHOSTNAME"
    echo "  cp $ARCHIVE/ETCHOSTS $ETCHOSTS"
  fi
  cp "$ARCHIVE/ETCHOSTNAME" "$ETCHOSTNAME"
  cp "$ARCHIVE/ETCHOSTS" "$ETCHOSTS"
  exit
fi

if [ "$verbose" == on -a "$change" == on ]; then
  echo "Old Hostname is ${HN}"
  echo "Old Domain is ${DOMAIN}"
  echo "New Hostname is ${NEWHOSTNAME}"
  echo "New Domain is ${NEWDOMAIN}"
fi

[ "$change" == on ] && set_hostname

# If we have no  arguments, we start a dialog
DIALOG=${DIALOG:-dialog}
which $DIALOG > /dev/null 2>&1 || exit
exec 3>&1
if [ $OPTIND -eq 1 ]; then
  while [ $exit == off ]; do
    result=$( $DIALOG --title "Hostname Manager" --clear \
      --menu "Choose Your Option:" 15 50 8 \
        "B" "Backup" \
        "X" "Restore" \
        "D" "Default" \
        "R" "Random Hostname and Domain" \
        "F" "Fixed Hostname and Domain" \
        "V" "Edit $ETCHOSTNAME and $ETCHOSTS" \
        "H" "Help" \
        "E" "Exit" 2>&1 1>&3 ) || \
      exit=on
    case "$result" in
    "B")
      if backup_dialog ; then
        mkdir -p $ARCHIVE && \
        cp "$ETCHOSTNAME" "$ARCHIVE/ETCHOSTNAME" && \
        cp "$ETCHOSTS" "$ARCHIVE/ETCHOSTS" && \
        "$DIALOG" --title "Backup" --msgbox "Backup Done" 6 40
      fi
      ;;
    "X")
      if restore_dialog ; then
        cp "$ARCHIVE/ETCHOSTNAME" "$ETCHOSTNAME" && \
        cp "$ARCHIVE/ETCHOSTS" "$ETCHOSTS" && \
        "$DIALOG" --title "Restore" --msgbox "Restore Done" 6 40
      fi
      ;;
    "D")
      if "$DIALOG" --title "Deafult" --yesno "Restore Default Hostname from ${HN}.${DOMAIN} to ${DEFAULT_HN}.${DEFAULT_DOMAIN}" 8 40; then
        NEWHOSTNAME="$DEFAULT_HN"
        NEWDOMAIN="$DEFAULT_DOMAIN"
        set_hostname && \
          "$DIALOG" --title "Default" --msgbox "Default Hostname was Restored" 6 40
        exit=on
      fi
      ;;
    "R")
      for FILE in "$ETCHOSTNAME" "$ETCHOSTS"; do
        if [ ! -r "$FILE" ]; then
         "$DIALOG" --title "Random Hostname" --msgbox "$FILE was not found. Aborting!" 6 40
          exit 2
        fi
      done
      random
      NEWHOSTNAME="$R"
      random
      NEWDOMAIN="$R"
     if "$DIALOG" --title "Random Hostname" --yesno "Change Hostname from ${HN}.${DOMAIN} to ${NEWHOSTNAME}.${NEWDOMAIN}?" 8 40; then 
      set_hostname && \
        "$DIALOG" --title "Random Hostname" --msgbox "Hostname changed from ${HN}.${DOMAIN} to ${NEWHOSTNAME}.${NEWDOMAIN}" 8 40
      exit=on
     fi
      ;;
    "F")
       NEWHOSTNAME="$( "$DIALOG" --title "Setting Hostname" --inputbox "New Hostname" 6 40 "$HN" 2>&1 1>&3 )" && \
         NEWDOMAIN="$( "$DIALOG" --title "Setting Domain" --inputbox "New Domain" 6 40 "$DOMAIN" 2>&1 1>&3 )" && \
         "$DIALOG" --title "Setting Hostname and Domain" --yesno \
         "Are you sure you want to change hostname from ${HN}.${DOMAIN} to ${NEWHOSTNAME}.${NEWDOMAIN}?" 8 60 && \
         set_hostname && \
         "$DIALOG" --title "Setting Hostname and Domain" --msgbox "Hostname changed from ${HN}.${DOMAIN} to ${NEWHOSTNAME}.${NEWDOMAIN}" 8 60 && \
         exit=on
       # Reset HOSTNAME and NEWDOMAIN as the entered values may be invalid (say when Cancel was selected)
       NEWHOSTNAME="$HN"
       NEWDOMAIN="$DOMAIN" 
      ;;
    "V")
      result=$( "$DIALOG" --title "View $ETCHOSTS" --editbox "$ETCHOSTS" 20 80 2>&1 1>&3 ) && \
        echo -e "$result" > $ETCHOSTS
      result=$( "$DIALOG" --title "View $ETCHOSTNAME" --editbox "$ETCHOSTNAME" 10 80 2>&1 1>&3 ) && \
        echo -e "$result" > $ETCHOSTNAME
      ;;
    "H")
      "$DIALOG" --title "Help" --msgbox "$USAGE" 35 85
      ;;
    "E")
      "$DIALOG" --title "Exit" --yesno "Are you sure that you want to Exit?" 6 40 && \
        exit=on
    ;;
    esac
  done
  exec 3>&-
fi
